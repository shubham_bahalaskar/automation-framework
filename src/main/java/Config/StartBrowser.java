package Config;
import org.testng.annotations.BeforeClass;
import io.github.bonigarcia.wdm.WebDriverManager;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.*;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class StartBrowser {
  public static WebDriver driver;
  public static ExtentReports extent;
  public static ExtentTest parentTest;
  public static ExtentTest childTest;
  ExtentSparkReporter sparkReporter;
  
  
  @BeforeTest
  public void generateReport() {
	  sparkReporter = new ExtentSparkReporter("Report/" + System.currentTimeMillis() +".html");
	  extent = new ExtentReports();
	  extent.attachReporter(sparkReporter);
  }

  
  
  @BeforeMethod
  public void methodName(Method method) {
	  parentTest = extent.createTest(method.getName());
  }
  
  @BeforeClass
  public void beforeClass() {
	  WebDriverManager.chromedriver().setup();
	  driver = new ChromeDriver();
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
  }

  @AfterClass
  public void afterClass() {
	  driver.quit();
	  extent.flush();
  }

}
