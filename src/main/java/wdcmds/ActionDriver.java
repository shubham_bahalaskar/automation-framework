package wdcmds;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.MediaEntityBuilder;

import Config.StartBrowser;

public class ActionDriver {
	public WebDriver driver;
	public ActionDriver() {
		driver = StartBrowser.driver;
	}
	
	
	/** 
	* used this to navigate to url.
	* @param url - URL of applicatin
	*/
	
	public void navigateToDriver(String url) {
		try {
			driver.get(url);
			StartBrowser.childTest.pass("Successfully Navigated to :"+url);
			
		}catch(Exception e) {
			StartBrowser.childTest.fail("Unable to Navigated to :"+url);
		}
	}
	
	/**
	 * Used to click on check box, button and link
	 * @param locator -- Get this from POM
	 * @param eleName -- Name of element
	 * @throws Exception -- 
	 */
	public void click(By locator, String eleName) throws Exception{
		try {
			driver.findElement(locator).click();
			StartBrowser.childTest.pass("performed click action on :"+ eleName);
		}catch (Exception e) {
			StartBrowser.childTest.fail("unable to click on :" + eleName, 
					MediaEntityBuilder.createScreenCaptureFromBase64String(getScreenshot()).build());
			StartBrowser.childTest.info(e);
			throw e;
		}
	}
	
	/**
	 * Used to perform send value to input. 
	 * @param locator -- get this from POM.
	 * @param testData -- test data from external files.
	 * @param eleName -- name of element.
	 */
	public void type(By locator, String testData, String eleName) throws Exception{
		try {
			driver.findElement(locator).sendKeys(testData);
			StartBrowser.childTest.pass("performed type action on :"+ eleName + "with test data" + testData);
		}catch(Exception e) {
			StartBrowser.childTest.fail("unable to type action on :"+ eleName + "with test data" + testData, 
					MediaEntityBuilder.createScreenCaptureFromBase64String(getScreenshot()).build());
			StartBrowser.childTest.info(e);
			throw e;
		}
	}
	
	public String getScreenshot() {
		return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);
		
	}
	
}
